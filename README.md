# Bastet USB DJ controller

Bastet is an Open Source DJ controller with some cool analog features.
The project is powered by [Zephyr OS](https://docs.zephyrproject.org), it makes full use of the Zephyr framework.

## Installation

The project is using Zephyr out-of-tree, it will require an installation of a Zephyr workspace somewhere on the user system described [here](https://docs.zephyrproject.org/latest/getting_started/index.html#get-zephyr-and-install-python-dependencies). After installation, remember to `export ZEPHYR_BASE=<your-workspace>/zephyr` to get `west` working.

## Hardware

The current `bastet` board (v0.0.1) is a clone of `nucleo_f103rb`, creating this board device-tree was more for educational purposes than anything else.
In the future the different boards configuration should be listed in `boards/<architecture>/bastet_vX.Y.Z`.

## Compilation

To compile for `bastet_vX.Y.Z` hardware, you will need to point Zephyr to this repository for boards searching. The compile commands then become:
```
west build --board=bastet_vX.Y.Z -- -DBOARD_ROOT=.
```

You can also compile this project for another board supported by Zephyr (makes sure that USB is supported on this board on [the boards list](https://docs.zephyrproject.org/latest/boards/index.html)).
```
west build -b <your-board>
```
